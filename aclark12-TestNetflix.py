#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from Netflix import find_pred
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_netflix_eval1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7115\n3.566\n3.532\nRMSE: 0.42\n")
    
    def test_netflix_eval2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.2874999999999996\n3.4605\n3.83\nRMSE: 0.94\n")
    
    def test_netflix_eval3(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10:\n3.2925\n3.16\nRMSE: 0.23\n")

    def test_find_pred_1(self):
        year = 2005
        movie = 1
        customer = 30878
        pred = find_pred(movie, customer, year)
        self.assertEqual(pred, 3.7115)

    def test_find_pred_2(self):
        year = 2005
        movie = 1
        customer = 2647871
        pred = find_pred(movie, customer, year)
        self.assertEqual(pred, 3.566)

    def test_find_pred_3(self):
        year = 2004
        movie = 1
        customer = 1283744
        pred = find_pred(movie, customer, year)
        self.assertEqual(pred, 3.532)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
